package io.alavanca.lacuna.app;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class LacunaApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Create a RealmConfiguration that saves the Realm file in the app's "files" directory.
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(this)
                .deleteRealmIfMigrationNeeded()
                .initialData(new InitialDataTransaction())
                .build();
        Realm.setDefaultConfiguration(realmConfig);

    }
}
