package io.alavanca.lacuna.app;

import android.util.Log;

import java.util.UUID;

import io.alavanca.lacuna.data.Story;
import io.realm.Realm;

/**
 * Created at 29/09/16.
 */

public class InitialDataTransaction implements Realm.Transaction {
    @Override
    public void execute(Realm realm) {
        realm.insertOrUpdate(new Story(UUID.randomUUID().toString(), "História 1", "fasodifjsaoidjf", "termo1;termo1_2"));
        Log.d(InitialDataTransaction.class.getSimpleName(), "1");
        realm.insertOrUpdate(new Story(UUID.randomUUID().toString(), "História 2", "fasodifjsaoidjf", "termo2;termo2_2"));
        realm.insertOrUpdate(new Story(UUID.randomUUID().toString(), "História 3", "fasodifjsaoidjf", "termo3;termo3_2;termo3_3;termo3_4"));
        realm.insertOrUpdate(new Story(UUID.randomUUID().toString(), "História 4", "fasodifjsaoidjf", "termo4;termo4_2"));
        realm.insertOrUpdate(new Story(UUID.randomUUID().toString(), "História 5", "fasodifjsaoidjf", "termo5;termo5_2;termo5_3"));
        realm.insertOrUpdate(new Story(UUID.randomUUID().toString(), "História 6", "fasodifjsaoidjf", "termo6;termo6_2"));
        realm.insertOrUpdate(new Story(UUID.randomUUID().toString(), "História 7", "fasodifjsaoidjf", "termo7;termo7_2"));
    }
}
