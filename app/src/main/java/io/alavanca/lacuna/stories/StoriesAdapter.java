package io.alavanca.lacuna.stories;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.alavanca.lacuna.R;
import io.alavanca.lacuna.data.Story;
import rx.Observable;
import rx.subjects.PublishSubject;

public class StoriesAdapter extends RecyclerView.Adapter<StoriesAdapter.ViewHolder> {
    private List<Story> stories;

    private final PublishSubject<Story> onClickSubject = PublishSubject.create();

    public StoriesAdapter(List<Story> stories) {
        this.stories = stories;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_title_story, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Story story = stories.get(position);

        holder.itemView.setOnClickListener(v -> onClickSubject.onNext(story));

        holder.titleStory.setText(stories.get(position).getTitle());
    }

    public Observable<Story> getPositionClicks() {
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return stories.size();
    }

    public void replaceData(List<Story> stories) {
        this.stories = stories;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView titleStory;

        public ViewHolder(View title) {
            super(title);
            titleStory = (TextView) title;
        }
    }
}
