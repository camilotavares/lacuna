package io.alavanca.lacuna.stories;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.alavanca.lacuna.R;
import io.alavanca.lacuna.data.Story;
import io.alavanca.lacuna.newstory.NewStoryActivity;
import rx.Observable;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class StoriesActivity extends AppCompatActivity implements StoriesContract.View {
    private RecyclerView listStories;
    private StoriesAdapter storiesAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private StoriesContract.Presenter presenter;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories);

        listStories = (RecyclerView)findViewById(R.id.stories_list);

        listStories.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        listStories.setLayoutManager(layoutManager);

        storiesAdapter = new StoriesAdapter(new ArrayList<>());
        listStories.setAdapter(storiesAdapter);

        presenter = new StoriesPresenter(this, new StoriesInteractor());
        presenter.start();
    }

    @Override
    public void showStories(List<Story> stories) {
        storiesAdapter.replaceData(stories);
    }

    @Override
    public Observable<Story> storyClick() {
        return storiesAdapter.getPositionClicks();
    }

    @Override
    public void openStoryCreator(Story story) {
        Intent intent = new Intent(this, NewStoryActivity.class);
        String message = story.getID().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }
}