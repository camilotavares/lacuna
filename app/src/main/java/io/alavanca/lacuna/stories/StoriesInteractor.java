package io.alavanca.lacuna.stories;

import java.util.List;

import io.alavanca.lacuna.data.Story;
import io.realm.Realm;
import rx.Observable;

public class StoriesInteractor implements StoriesContract.Interactor {

    private Realm realm;

    public StoriesInteractor() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public Observable<List<Story>> getStories() {

        return realm.where(Story.class).findAll().asObservable().map(stories -> ((List<Story>) stories));
    }

    @Override
    public void destroy() {
        realm.close();
    }
}
