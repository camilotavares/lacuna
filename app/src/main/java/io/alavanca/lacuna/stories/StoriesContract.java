package io.alavanca.lacuna.stories;

import java.util.List;

import io.alavanca.lacuna.data.Story;
import rx.Observable;

public class StoriesContract {

    public interface View {
        void showStories(List<Story> stories);

        Observable<Story> storyClick();

        void openStoryCreator(Story story);
    }

    public interface Presenter {
        void start();

        void destroy();
    }

    public interface Interactor {
        Observable<List<Story>> getStories();

        void destroy();
    }
}
