package io.alavanca.lacuna.stories;

import rx.Subscription;

public class StoriesPresenter implements StoriesContract.Presenter {
    private StoriesContract.View view;
    private StoriesContract.Interactor interactor;
    private Subscription interactorSubscribe, viewSubscribe;

    public StoriesPresenter(StoriesContract.View view, StoriesContract.Interactor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void start() {
        interactorSubscribe = interactor.getStories().subscribe(
                        stories -> {
                            view.showStories(stories);
                        }
                );

        viewSubscribe = view.storyClick().subscribe(
                story -> {
                    view.openStoryCreator(story);
                }
        );
    }

    @Override
    public void destroy() {
       // view = null;
        interactorSubscribe.unsubscribe();
        viewSubscribe.unsubscribe();
        interactor.destroy();
    }
}
