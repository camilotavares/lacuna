package io.alavanca.lacuna.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Story extends RealmObject{

    @PrimaryKey
    private String uuid;
    private String title;
    private String textStory;
    private String terms;

    public Story() {
    }

    public Story(String uuid, String title) {
        this.uuid = uuid;
        this.title = title;
    }

    public Story(String uuid, String title, String textStory, String terms) {
        this.uuid = uuid;
        this.title = title;
        this.textStory = textStory;
        this.terms = terms;
    }

    public Story(String title, String textStory, String terms) {
        this.title = title;
        this.textStory = textStory;
        this.terms = terms;
    }

    public Story(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getID() {
        return uuid;
    }

    public String getTextStory() {
        return textStory;
    }

    public String getTerms() {
        return terms;
    }

}
