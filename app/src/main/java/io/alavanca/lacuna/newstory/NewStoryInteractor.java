package io.alavanca.lacuna.newstory;

import io.alavanca.lacuna.data.Story;
import io.realm.Realm;

public class NewStoryInteractor implements NewStoryContract.Interactor {
    private Realm realm;

    public NewStoryInteractor() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public Story getStory(String id) {
        return realm.where(Story.class).equalTo("uuid", id).findFirst();
    }
}
