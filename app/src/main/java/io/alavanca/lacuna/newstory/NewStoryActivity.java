package io.alavanca.lacuna.newstory;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.alavanca.lacuna.R;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class NewStoryActivity extends AppCompatActivity implements NewStoryContract.View {
    private NewStoryContract.Presenter presenter;
    private TextView tvTerm;
    private Button btnNextTerm;
    private EditText edTerm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_story);

        tvTerm = (TextView) findViewById(R.id.tv_term_label);
        btnNextTerm = (Button) findViewById(R.id.btn_next);
        edTerm = (EditText) findViewById(R.id.ed_term);

        Bundle b = getIntent().getExtras();
        String id = b.getString(EXTRA_MESSAGE);

        presenter = new NewStoryPresenter(this, new NewStoryInteractor());
        presenter.start(id);
    }

    public void onNextTerm(View v){
        presenter.addTerm(edTerm.getText().toString());
    }

    @Override
    public void showNextTerm(String term) {
        edTerm.setText("");
        tvTerm.setText(term);
    }

    @Override
    public void lastStory() {
        btnNextTerm.setText("Finish");
    }

    @Override
    public void showUserStory() {
        Toast.makeText(this, "Acabou a história", Toast.LENGTH_LONG).show();
    }
}
