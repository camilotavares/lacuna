package io.alavanca.lacuna.newstory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.alavanca.lacuna.data.Story;

public class NewStoryPresenter implements NewStoryContract.Presenter {
    private NewStoryContract.View view;
    private NewStoryContract.Interactor interactor;
    private List<String> termList, termUserList;
    private int currentTerm;

    public NewStoryPresenter(NewStoryContract.View view, NewStoryContract.Interactor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void start(String id) {
        Story story = interactor.getStory(id);
        termList = Arrays.asList(story.getTerms().split(";"));
        termUserList = new ArrayList<>();
        onNextTerm();
    }

    @Override
    public void onNextTerm() {
        if (currentTerm < termList.size()-1) {
            view.showNextTerm(termList.get(currentTerm));
            System.out.println("onNextTerm - "+termList.get(currentTerm));
            currentTerm++;
        } else{
            view.showNextTerm(termList.get(currentTerm));
            view.lastStory();
            System.out.println("lastStory: "+termList.get(currentTerm));
        }
    }

    public void addTerm(String term) {
        termUserList.add(term);
        if (termUserList.size() >= termList.size()) {
            view.showUserStory();
        } else {
            onNextTerm();
        }
    }

    @Override
    public void destroy() {

    }
}
