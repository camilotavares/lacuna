package io.alavanca.lacuna.newstory;

import android.view.View;

import io.alavanca.lacuna.data.Story;

/**
 * Created by TRIAD on 20/09/2016.
 */

public class NewStoryContract {

    public interface View {
        void showNextTerm(String term);

        void lastStory();

        void showUserStory();
    }

    public interface Presenter {
        void start(String id);

        void onNextTerm();

        void addTerm(String term);

        void destroy();
    }

    public interface Interactor {
        Story getStory(String id);
    }
}
