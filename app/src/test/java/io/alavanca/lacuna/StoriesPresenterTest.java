package io.alavanca.lacuna;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import io.alavanca.lacuna.stories.StoriesContract;
import io.alavanca.lacuna.stories.StoriesPresenter;
import io.alavanca.lacuna.data.Story;
import rx.Observable;
import rx.subjects.PublishSubject;

import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StoriesPresenterTest {
    StoriesContract.View view = mock(StoriesContract.View.class);
    StoriesContract.Interactor interactor = mock(StoriesContract.Interactor.class);
    StoriesContract.Presenter presenter = new StoriesPresenter(view, interactor);
    PublishSubject<Story> onClickSubject = PublishSubject.create();
    List<Story> stories = Arrays.asList(new Story("louco"), new Story("doido"));



    @Test
    public void deve_mostrar_a_lista_de_stories() {
        when(interactor.getStories()).thenReturn(Observable.just(stories));
        when(view.storyClick()).thenReturn(Observable.empty());
        presenter.start();
        verify(view).showStories(anyList());
    }

    @Test
    public void deve_mostrar_o_item_quando_clicado() {
        when(interactor.getStories()).thenReturn(Observable.empty());
        when(view.storyClick()).thenReturn(onClickSubject);
        Story story = new Story("09809", "historia");
        presenter.start();
        onClickSubject.onNext(story);
        verify(view).openStoryCreator(story);
    }
}
