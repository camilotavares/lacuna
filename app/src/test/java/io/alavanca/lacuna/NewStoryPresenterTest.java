package io.alavanca.lacuna;

import android.view.View;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import io.alavanca.lacuna.data.Story;
import io.alavanca.lacuna.newstory.NewStoryContract;
import io.alavanca.lacuna.newstory.NewStoryPresenter;
import rx.subjects.PublishSubject;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NewStoryPresenterTest {
    NewStoryContract.View view = mock(NewStoryContract.View.class);
    NewStoryContract.Interactor interactor = mock(NewStoryContract.Interactor.class);
    NewStoryContract.Presenter presenter = new NewStoryPresenter(view, interactor);
    PublishSubject<Story> onClickSubject = PublishSubject.create();
    List<Story> stories = Arrays.asList(
            new Story("01", "Primeira estória", "Este texto gosta de <tag>termo</tag> e de fica <tag>termo_2</tag>", "termo;termo_2;termo_3;term_4;term_5"),
            new Story("02", "Segunda estória", "Este texto também é <tag>termo</tag> e <tag>termo_2</tag>", "termo;termo_2"));

    @Test
    public void deve_ir_para_o_proximo_termo_quando_next_for_clicado() {
        when(interactor.getStory("01")).thenReturn(stories.get(0));
        presenter.start("01");
        List<String> terms = Arrays.asList(stories.get(0).getTerms().split(";"));
        verify(view).showNextTerm(terms.get(0));
        for (String term : terms) {
            presenter.onNextTerm();
            verify(view).showNextTerm(term);
        }
    }
}
